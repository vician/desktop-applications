#!/bin/bash

type="" #beta,devedition,nightly
mtype=""

if [ $# -ne 0 ]; then
	type=$1
	if [ $type != "beta" ] && [ $type != "devedition" ] && [ $type != "nightly" ]; then
		echo "Wrong type of firefox!"
		exit 1
	fi
	mtype="-$type"
fi

dir="$HOME/.local/firefox$mtype/"

if [ ! -f ${dir}firefox ]; then

	#https://download.mozilla.org/?product=firefox-latest-ssl&os=linux64&lang=en-US
	#https://download.mozilla.org/?product=firefox-beta-latest-ssl&os=linux64&lang=en-GB

	# *.tar.bz2
	url="https://download.mozilla.org/?product=firefox$mtype-latest-ssl&os=linux64&lang=en-GB"
	archive=$(mktemp /tmp/firefox$mtype.tar.bz2.XXXXXX)
	unwrap=$(mktemp -d /tmp/firefox$mtype.XXXXXX)

	wget -O $archive $url
	if [ $? -ne 0 ]; then
		echo "Cannot download $url!"
		exit 1
	fi

	tar -xjv -f $archive -C $unwrap
	if [ $? -ne 0 ]; then
		echo "Cannot unwrap $archive!"
		exit 1
	fi

	rm -rf $dir
	mkdir -p $dir

	mv $unwrap/firefox/* $dir/
	if [ $? -ne 0 ]; then
		echo "Cannot deploy firefox to $dir!"
		exit 1
	fi
fi

desktop="$HOME/.local/share/applications/firefox$mtype.desktop"
cp /usr/share/applications/firefox.desktop $desktop
if [ $? -ne 0 ]; then
	echo "Cannot deploy firefox desktop file to $desktop!"
	exit 1
fi

case $type in
	"beta")
		iconurl="https://design.firefox.com/product-identity/firefox-beta/firefox-logo-beta.svg"
		;;
	"devedition")
		iconurl="https://design.firefox.com/product-identity/firefox-developer-edition/firefox-logo-developer-edition.svg"
		;;
	"nightly")
		iconurl="https://design.firefox.com/product-identity/firefox-nightly/firefox-logo-nightly.svg"
		;;
	*)
		iconurl="https://design.firefox.com/product-identity/firefox/firefox/firefox-logo.svg"
		;;
esac

if [ ! -f $dir/firefox$mtype.svg ]; then
	wget -O $dir/firefox$mtype.svg $iconurl
	if [ $? -ne 0 ]; then
		echo "Cannot download new icon from $iconurl!"
		exit 1
	fi
fi

profile="$type"
if [ "$profile" ]; then
	grep "Name=$profile" $HOME/.mozilla/firefox/profiles.ini
	if [ $? -ne 0 ]; then
		# Profile doesn't exist yet
		firefox -CreateProfile $profile
		if [ $? -ne 0 ]; then
			echo "Cannot create new firefox profile!"
			exit 1
		fi
	fi
	sed -i "s/Exec=firefox/Exec=firefox -P $profile/g" $desktop
	if [ $? -ne 0 ]; then
		echo "Cannot update profile!"
		exit 1
	fi
fi

sed -i "s/Exec=firefox/Exec=firefox --class=firefox$mtype --no-remote/g" $desktop
if [ $? -ne 0 ]; then
	echo "Cannot update class!"
	exit 1
fi

sed -i "s/Name=Firefox/Name=Firefox ${type^}/g" $desktop
if [ $? -ne 0 ]; then
	echo "Cannot update name!"
	exit 1
fi

sed -i "s|Exec=firefox|Exec=${dir}firefox|g" $desktop
if [ $? -ne 0 ]; then
	echo "Cannot update bin path!"
	exit 1
fi

sed -i "s|Icon=firefox|Icon=${dir}firefox$mtype.svg|g" $desktop
if [ $? -ne 0 ]; then
	echo "Cannot update icon!"
	exit 1
fi
